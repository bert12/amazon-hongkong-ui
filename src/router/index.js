import Vue from 'vue'
import Router from 'vue-router'
import login from '@/pages/login/index'

const sortingConfig = () =>
    import ('@/pages/vendor_management/sortingConfig.vue')

Vue.use(Router)
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
const router = new Router({

    routes: [{
        path: '/',
        redirect: '/login'
    }, {
        path: '/login',
        name: 'login',
        component: login => require(['@/pages/login/index'], login)
    }, {
        path: '/home',
        component: home => require(['@/pages/home/index'], home),
        meta: { authority: '/home', keepAlive: false },
        children: [{
                path: '/sorting-config',
                name: 'sortingConfig',
                meta: { authority: '/home', keepAlive: false },
                component: sortingConfig //供应商配置
            },
            {
                path: '/ades/manifest-list',
                name: 'manifestList',
                meta: { authority: '/home', keepAlive: false },
                component: () =>
                    import ('@/pages/ades/manifestList.vue')
            },
            {
                path: '/ades/package-list',
                name: 'packageList',
                meta: { authority: '/home', keepAlive: false },
                component: () =>
                    import ('@/pages/ades/packageList.vue')
            },
            {
              path: '/ades/job-list',
              name: 'jobList',
              meta: { authority: '/home', keepAlive: false },
              component: () =>
                  import ('@/pages/ades/jobList.vue')
          },
        ]
    }]
})

export default router