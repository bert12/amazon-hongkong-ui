import * as types from '../mutation-types'
import api from '@/http'
import { mGetDate , sortCompare} from '@/utils/fun'
import { getPromiseAction ,getPromiseActionNoMutations} from '@/utils/promiseUtils'
import { stat } from 'fs'

export default {
  name:'basic',
  namespaced: true,
  state() {
    return {
    }
  },
  getters: {
    
  },
  mutations: {
    getChannels(state,body){
    },
    [types.GET_COMPARATIVE_RELATION](state,body){
    
    },
  },
  actions: {
    loadCodeQuery({commit},payload){
      return getPromiseAction (api.codeQuery(payload),commit,types.CODE_QUERY)
    },
    loadCodeCreate({commit},payload){
      return getPromiseActionNoMutations (api.codeCreate(payload))
    }

  }
}