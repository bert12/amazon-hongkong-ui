export default {
    header: {
        "CLICK_ON": "点击展开",
        "CLICK_ON_THE_FOLD": "点击收起"
    },
    menu: {
        "VENDORS_CONFIG": "供应商配置",
        "VENDORS_MANAGEMENT": "供应商管理",
        "1": "basic list",
        "1_1": "Manifest",
        "1_2": "Job",
        "1_3": "Package",
        "2": "分拣设置",
        "2_1": "分拣方案",
        "3": "分拣报告",
        "3_1": "批次报告",
        "3_2": "分拣日报",
        "3_3": "效率数据",
        "3_4": "移交报告",
        "4": "应急方案",
        "4_1": "导入作业数据",
        "4_2": "同步数据状态",
        "5": "通讯监控",
    },
    ades: {
        "NUMDER": 'Manifest NO',
        "CUSTOMER": 'Customer',
        "SHIPMENT_DATE": 'Shipment Date',
    }
}